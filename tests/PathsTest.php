<?php

namespace Slts\Includer;

use PHPUnit\Framework\TestCase;
use Slts\Includer\Exceptions\PathNotFoundException;

class PathsTest extends TestCase
{
    public function testBuild()
    {
        list($paths, $data) = $this->createPaths();

        $sep = DIRECTORY_SEPARATOR;
        $result = "{$data['dev']}{$sep}foo";
        self::assertSame($result, $paths->build('dev', '/foo'));
        self::assertSame($result, $paths->build('dev', '\foo'));
        self::assertSame($result, $paths->build('dev', 'foo'));
    }

    public function testGet()
    {
        list($paths, $data) = $this->createPaths();

        self::assertSame($data['dev'], $paths->get('dev'));
        self::assertSame($data[0], $paths->get(0));
        self::assertSame('/var', $paths->get('front'));
        self::assertSame('C:\var', $paths->get('back'));

        $this->expectException(PathNotFoundException::class);
        $paths->get('foo');
    }

    private function createPaths()
    {
        $data = [
            'dev' => '/dev/null',
            '/var/temp/cache',
            'front' => '/var/',
            'back' => 'C:\var\\',
        ];
        return [new Paths($data), $data];
    }
}
