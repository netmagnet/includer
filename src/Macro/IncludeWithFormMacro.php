<?php

namespace Slts\Includer\Macro;

use Latte\CompileException;
use Latte\Helpers;
use Latte\IMacro;
use Latte\Macro;
use Latte\MacroNode;
use Latte\PhpWriter;

class IncludeWithFormMacro implements Macro
{
    const MACRO_NAME = 'includeWithForm';

    public function nodeOpened(MacroNode $node): bool
    {
        $node->empty = true;
        $node->replaced = false;

        $destination = $node->tokenizer->fetchWord(); // destination [,] [params]
        if (!preg_match('~#|[\w-]+\z~A', $destination)) {
            return false;
        }

        $destination = ltrim($destination, '#');
        $parent = $destination === 'parent';
        if ($destination === 'parent' || $destination === 'this') {
            for ($item = $node->parentNode; $item && $item->name !== 'block' && !isset($item->data->name); $item = $item->parentNode);
            if (!$item) {
                throw new CompileException("Cannot include $destination block outside of any block.");
            }
            $destination = $item->data->name;
        }

        $noEscape = Helpers::removeFilter($node->modifiers, 'noescape');
        if (!$noEscape && Helpers::removeFilter($node->modifiers, 'escape')) {
            trigger_error('Macro ' . $node->getNotation() . ' provides auto-escaping, remove |escape.');
        }
        if ($node->modifiers && !$noEscape) {
            $node->modifiers .= '|escape';
        }
        $writer = PhpWriter::using($node);
        $node->openingCode = $writer->write(
            '<?php $this->renderBlock' . ($parent ? 'Parent' : '') . '('
            . (strpos($destination, '$') === false ? var_export($destination, true) : $destination)
            . ', %node.array? + '
            . (isset($this->namedBlocks[$destination]) || $parent ? 'get_defined_vars()' : '$this->params')
            . ' + [\'tform\' => end($this->global->formsStack)]'
            . ($node->modifiers
                ? ', function ($s, $type) { $_fi = new LR\FilterInfo($type); return %modifyContent($s); }'
                : ($noEscape || $parent ? '' : ', ' . var_export(implode($node->context), true)))
            . '); ?>'
        );
        return true;
    }

    public function nodeClosed(MacroNode $node)
    {
    }

    public function initialize()
    {
    }

    public function finalize()
    {
    }
}
