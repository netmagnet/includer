<?php

namespace Slts\Includer\Macro;

use Latte\CompileException;
use Latte\Helpers;
use Latte\Macro;
use Latte\MacroNode;
use Latte\PhpWriter;
use Slts\Includer\Paths;

class IncludePathMacro implements Macro
{
    const MACRO_NAME = 'includePath';

    private $paths;

    public function __construct(
        Paths $paths
    ) {
        $this->paths = $paths;
    }

    public function nodeOpened(MacroNode $node): bool
    {
        $node->empty = true;
        $node->replaced = false;

        $package = $node->tokenizer->fetchWord();
        if (!$package) {
            throw new CompileException("Missing package in {$node->name}.");
        }

        $destination = $node->tokenizer->fetchWord();
        if (!$destination) {
            throw new CompileException("Missing template name in {$node->name}.");
        }

        $finalDestination = $this->paths->build($package, $destination);

        $noEscape = Helpers::removeFilter($node->modifiers, 'noescape');
        if (!$noEscape && Helpers::removeFilter($node->modifiers, 'escape')) {
            trigger_error('Macro ' . $node->getNotation() . ' provides auto-escaping, remove |escape.');
        }
        if ($node->modifiers && !$noEscape) {
            $node->modifiers .= '|escape';
        }
        $writer = PhpWriter::using($node);

        $node->openingCode = $writer->write(
            '<?php /* line ' . $node->startLine . ' */
            echo $this->createTemplate(\'' . $finalDestination . '\', %node.array? + $this->params, "include")->renderToContentType(%raw); ?>',
            $node->modifiers
                ? $writer->write('function ($s, $type) { $_fi = new LR\FilterInfo($type); return %modifyContent($s); }')
                : var_export($noEscape ? null : implode($node->context), true)
        );
        return true;
    }

    public function nodeClosed(MacroNode $node)
    {
    }

    public function initialize()
    {
    }

    public function finalize()
    {
    }
}
