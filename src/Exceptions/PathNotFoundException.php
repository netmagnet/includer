<?php

namespace Slts\Includer\Exceptions;

use OutOfRangeException;

class PathNotFoundException extends OutOfRangeException
{
}
