<?php

namespace Slts\Includer\DI;

use Nette\DI\CompilerExtension;
use Slts\Includer\Macro\ImportPathMacro;
use Slts\Includer\Macro\IncludeWithFormMacro;
use Slts\Includer\Macro\IncludePathMacro;
use Slts\Includer\Paths;

class IncluderExtension extends CompilerExtension
{
    private $defaults = [
        'paths' => [],
    ];

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $this->validateConfig($this->defaults);

        $builder
            ->addDefinition($this->prefix('paths'))
            ->setFactory(Paths::class)
            ->setArguments([$this->config['paths']])
        ;
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        $latte = $builder->getDefinition('latte.latteFactory')->getResultDefinition();

        $latte
            ->addSetup(
                '?->onCompile[] = function ($engine) { $engine->getCompiler()->addMacro("' . IncludePathMacro::MACRO_NAME . '", new ' . IncludePathMacro::class . '(?)); }',
                ['@self', $builder->getDefinition($this->prefix('paths'))]
            )
        ;
        $latte
            ->addSetup(
                '?->onCompile[] = function ($engine) { $engine->getCompiler()->addMacro("' . ImportPathMacro::MACRO_NAME . '", new ' . ImportPathMacro::class . '(?)); }',
                ['@self', $builder->getDefinition($this->prefix('paths'))]
            )
        ;
        $latte
            ->addSetup(
                '?->onCompile[] = function ($engine) { $engine->getCompiler()->addMacro("' . IncludeWithFormMacro::MACRO_NAME . '", new ' . IncludeWithFormMacro::class . '()); }',
                ['@self']
            )
        ;
    }
}
