<?php

namespace Slts\Includer;

use Slts\Includer\Exceptions\PathNotFoundException;

class Paths
{
    protected $paths;

    public function __construct(array $paths)
    {
        $this->paths = $this->normalizePaths($paths);
    }

    public function get($name)
    {
        if ($this->paths[$name] ?? false) {
            return $this->paths[$name];
        }
        throw new PathNotFoundException("Path {$name} not found. Did you defined it in config?");
    }

    public function build($name, $path)
    {
        $path = ltrim($path, '\/');
        return $this->get($name) . DIRECTORY_SEPARATOR . $path;
    }

    private function normalizePaths(array $paths)
    {
        return array_map(function (string $path) {
            return rtrim($path, '\/');
        }, $paths);
    }
}
