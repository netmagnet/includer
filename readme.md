# Includer

## Configuration
```
extensions:
    includer: Slts\Includer\DI\IncluderExtension

includer:
    paths:
        app: "%appDir%/../src/App/templates"
        components: "%appDir%/../src/myModule/templates/components"
```

## Usage
```
# Easy including other templates without need of resolving relative path
{includePath app foo.latte}
{importPath components bar.latte}

# Improved including defined blocks with form passed in args
# Usage is limited for inside of {form} or {formContainer} macro
{includeWithForm foo}
is replacement for
{include foo, form => $form}
```
